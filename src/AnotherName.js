import React from 'react'

const AnotherName = (props) => {
    return (
        <a onClick={() => {props.handleClick()}} className='button'>
            Give me another band name
        </a>
    )
}

export default AnotherName