import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import firebase from '@firebase/app';
import '@firebase/firestore';
import { FirestoreProvider } from "react-firestore";

const config = {
    apiKey: "AIzaSyDIgMNDorFNuBeK6wQEN23DiLld3ThEc7A",
    authDomain: "good-name-for-a-band.firebaseapp.com",
    projectId: 'good-name-for-a-band'
  };

firebase.initializeApp(config);

ReactDOM.render(
<FirestoreProvider firebase={firebase}>
    <App />
</FirestoreProvider>, document.getElementById('root'));
registerServiceWorker();
