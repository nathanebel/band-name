import React, { Component } from 'react';
import firebase from '@firebase/app';
import '@firebase/firestore';
import { FirestoreCollection } from "react-firestore";
import './App.css';
import 'react-typist/dist/Typist.css';
import VideoBackground from './VideoBackground.js';
import BandName from './BandName.js';
import SubmitName from './SubmitName.js';
import Loading from './Loading.js';
const shuffle = require('shuffle-array')


// const bandNamesArray = []

// db.collection("band-names").get().then(function(querySnapshot) {
//   querySnapshot.forEach(function(doc) {
//       // doc.data() is never undefined for query doc snapshots
//       console.log(doc.data().name)
//       bandNamesArray.push(doc.data().name)
//       console.log(bandNamesArray)
//   })
// });


class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isLoaded : false
    }
  }

  handleVideoLoaded = () => {
    this.setState({ isLoaded: true }, function() {
        console.log(this.state)
      });       
    }

  render() {
    console.log('app.js state')
    console.log(this.state)

  //   db.collection("band-names").add({
  //     name: 'Struggle Session',
  // })
  // .then(function(docRef) {
  //     console.log("Document written with ID: ", docRef.id);
  // })
  // .catch(function(error) {
  //     console.error("Error adding document: ", error);
  // });

    return (
      <div>
          <FirestoreCollection 
            path="band-names"
            render={({ isLoading, data }) => {
              // Fetch our data from firebase

              return isLoading ? (
                  <Loading loading={isLoading} />
                  // If we aren't done loading the data, display the load screen
              ) : (
                // If the data loads, display the new band name
                <div>
                  <BandName allBandNames={shuffle(data)} />
                </div>
              )
            }}
          />
          <VideoBackground loaded={this.state.isLoaded} handleVideoLoaded={this.handleVideoLoaded} />
      </div>
    )
  }
}

export default App; 