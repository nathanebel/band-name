import React from 'react'

const Loading = (props) => {
    return (
        <div className={props.loading + ' loaderContainer'}></div>
    )
}

export default Loading