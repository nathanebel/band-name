import Typist from 'react-typist'
import React, { Component } from 'react'
import AnotherName from './AnotherName.js'
import SubmitName from './SubmitName.js'
const shuffle = require('shuffle-array')

class BandName extends Component {

    constructor (props) {
        super(props)

        this.state = {
            bandNames : shuffle(this.props.allBandNames)
        }

        console.log(this.state.bandNames)

    }

    handleAnotherName = () => {
        console.log('handling click')
        // this.setState({
        //     currentBand : randomBand()
        // })

    }

    render () {
        
        return (
        <div className="interactiveWrapper">
            <div className="bandNameWrapper">
                <Typist avgTypingDelay={90}>
                    <Typist.Delay ms={1500} />
                    <span className="currentBandName">
                        {this.state.bandNames[0].name}
                    </span>
                </Typist>
            </div>
            <AnotherName handleClick={this.handleAnotherName} />
            <SubmitName />
        </div>
        )
    }
}

export default BandName