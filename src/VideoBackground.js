import backgroundVideo from './assets/low.mp4' ;
import backgroundImage from './assets/video-frame.png';
import React from 'react'

const VideoBackground = (props) => {
    console.log(this)
    console.log(props)
    return (
    <div className={props.loaded === true ? 'videoVisible' : ''}>
        <div className="background-cover"></div>
        <video id="background-video" 
                poster={backgroundImage} 
                loop 
                muted 
                autoPlay
                onLoadedData={props.handleVideoLoaded}
                >
            <source src={backgroundVideo} type="video/mp4" />
        </video>
    </div>
    )
}

export default VideoBackground