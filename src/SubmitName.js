import React from 'react'

const SubmitName = () => {
    return (
        <a className='button'>
            Submit a band name
        </a>
    )
}

export default SubmitName